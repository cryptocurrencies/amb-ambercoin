# ---- Base Node ----
FROM ubuntu:16.04 AS base
RUN apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 8842ce5e && \
    echo "deb http://ppa.launchpad.net/bitcoin/bitcoin/ubuntu xenial main" > /etc/apt/sources.list.d/bitcoin.list
RUN apt-get update && \
    apt-get install -y build-essential libtool autotools-dev autoconf libleveldb-dev libssl-dev git-core libboost-all-dev libdb4.8-dev libdb4.8++-dev libminiupnpc-dev pkg-config sudo && \
    apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

# ---- Build Source ----
FROM base as build
RUN git clone https://github.com/AmberCoinDev/Amber.git /opt/ambercoin && \
	cd /opt/ambercoin/src/leveldb && \
	chmod +x build_detect_platform && \
	make libleveldb.a libmemenv.a && \
    cd /opt/ambercoin/src && \
    make -f makefile.unix

# ---- Release ----
FROM ubuntu:16.04 AS release
RUN apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 8842ce5e && \
    echo "deb http://ppa.launchpad.net/bitcoin/bitcoin/ubuntu xenial main" > /etc/apt/sources.list.d/bitcoin.list
RUN apt-get update && \
    apt-get install -y libboost-all-dev libdb4.8 libdb4.8++ libminiupnpc-dev && \
    apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*
RUN groupadd -r ambercoin && useradd -r -m -g ambercoin ambercoin
RUN mkdir /data
RUN chown ambercoin:ambercoin /data
COPY --from=build /opt/ambercoin/src/AmberCoind /usr/local/bin/
USER ambercoin
VOLUME /data
EXPOSE 39347 39348
CMD ["/usr/local/bin/AmberCoind", "-datadir=/data", "-conf=/data/AmberCoin.conf", "-server", "-txindex", "-printtoconsole"]